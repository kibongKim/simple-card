﻿using System.Collections.Generic;

using UnityEngine;

public class GameManager : MonoSingleton<GameManager>
{
    public Vector2Int cardSize;
    public GameObject card;

    private Card[] cards = null;
    private bool isStart = false;

    private void Awake()
        => cards = new Card[cardSize.x * cardSize.y];

    private void OnGUI()
    {
        if (GUI.Button(
            new Rect(Screen.width - (24f + 96f), Screen.height - (24f + 68f), 96f, 68f),
            isStart ? "ReStart" : "Start"))
        {
            if (isStart)
            {
                UpdateManager.Clear();
                UserController.Get.Reset();

                for (int i = 0; i < cards.Length; ++i)
                    cards[i].Reset();
            }

            GameStart();
        }
    }

    private void GameStart()
    {
        List<Color> colors = new List<Color>(cardSize.x * cardSize.y);
        RandomColor(colors, cards.Length / 2);

        colors.AddRange(colors);
        Shuffle(colors, colors.Count);

        int i = 0;
        for (int y = 0; y < cardSize.y; ++y)
        {
            for (int x = 0; x < cardSize.x; ++x)
            {
                if (cards[i] == null)
                {
                    GameObject go = Instantiate(card, transform);
                    go.transform.localPosition = new Vector3(x * 2f, y * -2.25f, 0f);

                    cards[i] = go.GetComponent<Card>();
                }

                cards[i].CardColor = colors[i++];
            }
        }

        isStart = true;
    }

    private void RandomColor(List<Color> list, int count)
    {
        if (count == 0) return;

        Color color = new Color
            (
                Random.Range(0f, 1f),
                Random.Range(0f, 1f),
                Random.Range(0f, 1f)
            );

        if (list.Exists(x => x.Equals(color))) RandomColor(list, count);
        else
        {
            list.Add(color);
            RandomColor(list, count - 1);
        }
    }

    //  Fisher-Yates shuffle
    //  https://en.wikipedia.org/wiki/Fisher–Yates_shuffle
    private void Shuffle(List<Color> list, int count)
    {
        while (count-- > 1)
        {
            int d = Random.Range(0, count);

            Color color = list[d];
            list[d] = list[count];
            list[count] = color;
        }
    }

}
