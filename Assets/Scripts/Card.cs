﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    private SpriteRenderer frontCardRenderer;
    private SpriteRenderer cardRenderer;

    private AudioSource audioSource;
    private bool isEnable = true;

    public Color CardColor
    {
        get => cardRenderer.color;
        set => cardRenderer.color = value;
    }

    private void Awake()
    {
        frontCardRenderer = GetComponent<SpriteRenderer>();
        cardRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();

        audioSource = GetComponent<AudioSource>();
    }

    public void Reset()
    {
        isEnable = true;
        gameObject.SetActive(true);
        transform.rotation = Quaternion.identity;

        frontCardRenderer.color = new Color(1f, 1f, 1f, 1f);
    }

    public void RotateAnimation(bool isUp)
    {
        if (!isEnable) return;

        UpdateManager.Add(UpdateRotate(isUp, GameOption.CARD_ROTATE_TIME));
        audioSource.Play();
    }

    public void DeleteAnimation()
    {
        if (!isEnable) return;

        UpdateManager.Add(UpdateOpacity(GameOption.CARD_OPACITY_TIME));
    }

    private IEnumerator UpdateRotate(bool isUp, float time)
    {
        float currTime = Time.time;
        Vector3 vecAngle = isUp ? Vector3.up : Vector3.zero;

        Quaternion prev = isUp ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f);
        Quaternion next = isUp ? Quaternion.Euler(0f, 180f, 0f) : Quaternion.identity;

        while (Time.time - currTime <= time)
        {
            transform.rotation = Quaternion.Lerp(
                prev,
                next,
                (Time.time - currTime) / time);

            yield return null;
        }

        transform.rotation = next;
        yield break;
    }

    private IEnumerator UpdateOpacity(float time)
    {
        isEnable = false;

        float currTime = Time.time;
        Color color = CardColor;

        while (Time.time - currTime <= time)
        {
            color.a = Mathf.Lerp(
                1f, 0f,
                (Time.time - currTime) / time);

            CardColor = color;
            frontCardRenderer.color = new Color(1f, 1f, 1f, color.a);

            yield return null;
        }

        color.a = 0f;
        CardColor = color;
        frontCardRenderer.color = new Color(1f, 1f, 1f, color.a);

        gameObject.SetActive(false);
        yield break;
    }

}
